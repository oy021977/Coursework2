ABSTRACT

This document contains a Product Breakdown Structure for my Computer Science BSc degree. It also contains a
Gantt chart which focuses on my own learning plan and the number of working hours required for each module to achieve my degree.
Furthermore, the document has a CV which contains information about myself, my work history and experience over the past few years, 
the skillset I have gained. The CV also contains my education background and the Certificates I have earned thus far. The document 
also contains a reflective section explaining what I have learned upon completing this project, and how my learning method changed
from my college years to university.

INTRODUCTION

A Plan Breakdown Structure (PBS) is a hierarchical outline of the tasks needed to deliver the project’s product or service.  
It “breaks-down” the project into low-level subtask units of work that will be scheduled, executed and controlled.
A Gantt chart was used which focuses on the management and allocation of time to different tasks, and the allocated duration to complete the tasks,
before moving to another stage

CV

[CV_1.pdf](/uploads/1eb236e3c0d7bdab2741944e1dc8b8b5/CV_1.pdf)

Name: Baboucarr Mbye
Address: 4 Crown Place(off London Road) , Reading , RDG RG1 5AE
mobile no: 07436921297, email: bbmbye@gmail.com

Professional Summary: I am a young dedicated Student, aspiring to become a Computer Scientist.

Skills: 
Fast Learner 
Highly Dependable
Good Team player
Basic Programming skills
Excellent with Microsoft Office
Basic C language
Good at Formatting and Installing Software
Able to configure Routers

Language Skills: Fluent in English; Fluent in Wollof.

Work History:
06/2017 to 08/2017 
IT Technician Africell (GSM Operator) – Banjul, The Gambia.
My role during the internship was to format and install new operating systems on computers. 
On occasions, when PC's or computers crashed, I was tasked with repairing and booting a new Operating system by either using a CD or a Hard drive,
to run the software. Furthermore, I did network related jobs such as configuring routers and access points for other businesses,
that paid for Africell to provide them with Internet access.

06/2015 to 08/2015
Call Centre Agent Africell Call Centre – Banjul, The Gambia.
At the completion of my IGCSE exams, I took a job at the Africell Call Centre. Here I was responsible for receiving customer complaints with
problems regarding various areas such as: Poor network connection; slow Internet connection;
Invalid Credit; Stolen mobile phones; Sim card issues; and many more problems.

Education:
2014
GCSE: Marina International School - Banjul, The Gambia.
Upon graduation I received the International General Certificate of Secondary Education.
Graduated among the top 10% of students that year in my school, and achieved results of 4 A' s and 4 B' s.

2018
A-Levels: Maths, Chemistry And Physics Abbey College Manchester.  
At the completion of my course, I received the A-level certificate, 
and with those grades went on to apply for a University place to further my education.

References:

Mr Kekura Mansaray
Exams Officer/ Maths Teacher
Marina International School, Farming Singhateh Street, Bakau New Town, Banjul, The Gambia
Tel no: +220 449 7178
Email: kekuramansaray@yahoo.com

Mr Ebrima Barry
Administration & Logistic Director
Africell Gambia Company Lmited
43 Kairaba Avenue, Banjul, The Gambia
Phone: +220 437 6022
Fax: +220 437 6044

Mr Assan Mbye
Finance Director
Africell Gambia Company Lmited
43 Kairaba Avenue, Banjul, The Gambia
Phone: +220 775 0034
Tel no: +220 740 0000
Fax: +220 437 6066
Email: asmbye@africell.gm


PRODUCT BREAKDOWN STRUCTURE

![Computer_Science](/uploads/428485bcb9d1eaa57a0f7b67158d2e3c/Computer_Science.jpg)


GANTT CHART

![GANTT_CHART](/uploads/d680cdaf6a1971551b8a4176b91ebd31/GANTT_CHART.png)

REFLECTIONS

The objective I want to achieve by the end of my 3 years study at the University of Reading is a
Batchelor's degree in Computer Science.
In this Coursework, I have learnt that planning your work before you begin, helps with time allocation, and being able to divide labour.
It also helps underline the important tasks and the difficult ones, thus enabling me to tackle these problems in the right manner. During my years in college,
planning was not something I was fond of, instead I will do the project with no schedule, or awareness of how important some tasks are and how much
they contribute to the final grade. Poor estimation of the difficulty of some tasks lead to spending majority of my time focusing on menial work, 
whiles more daunting tasks were left until late, this lead to underachieving in certain areas and not getting the expected grades.

In university I quickly realised that the method in which I went about with my studies during college and high school, will not be sufficient to succeed.
I knew that i needed to develop my studying strategies to adapt to my new academic surroundings. From my experience thus far, university requires more
independent work and taking full ownership of your learning.
The steps I have taken from my college and high school days to develop my learning are:
 Before a lecture I read ahead and try to have a general idea on what topics are going to be covered on that day
I borrowed few Library books to support me with my learning and to have more than one view of looking at things, this is a huge step up compared to my college days,
when I only read what was given to me by the teacher and did not further my research on topics.
 Frequent use of websites relating to my course.
 A calender that reminds me of upcoming deadline.




